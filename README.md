piWiFiCtrl

A collection programs running on the Raspberry Pi Raspbian OS to enable convenient headless operation and Wi-Fi connections.

* LinuxServer_piWiFiCtrl:  A C++ program that runs as a daemon.  It monitors the Wi-Fi state and automatically switches the pi to router mode when no local networks are available, or connects to a known Wi-Fi network when one becomes visible

* AspNet_piWiFiCtrl:  A simple web application to control the Wi-Fi connection state. This includes a page to select available Wi-Fi access points and enter the connection password

* rpi:  A folder with various configuration files from the Raspbian OS that are modified to support the system operation.
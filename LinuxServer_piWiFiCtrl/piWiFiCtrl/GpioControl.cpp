#include <wiringPi.h>
#include <list>

#include "GpioControl.h"
#include "SimpleTimer.h"

using namespace std;

list<PinController*> Pins;
PinController* PinConnectionStatus;


bool Running = false;

// Timer task running to update PIN states
//
void TimerTask()
{
	while (Running)
	{
		delay(5);
	
		for (auto nextPin = Pins.begin(); nextPin != Pins.end(); ++nextPin)
		{
			(*nextPin)->Update();
		}
	}
	
	//  shut down
	for (auto nextPin = Pins.begin(); nextPin != Pins.end(); ++nextPin)
	{
		(*nextPin)->Switch(false);
	}
}


//  Start the controller
//  Initialize wiringPi and begin the timer task 
void StartGpioController(int pinConnection)
{
	if (pinConnection > 0 )
		wiringPiSetupPhys();
	
	PinConnectionStatus = new PinController(pinConnection);

	
	if (pinConnection > 0 )
	{
		Running = true;
		
		if (pinConnection > 0)
		{
			Pins.push_back(PinConnectionStatus);
		}
					
		SimpleTimer startTimerTask(0, true, &TimerTask);
	}
}


//  Stop the controller
//
void StopGpioController()
{
	Running = false;
}

void SetConnectionLed(RouterState state)
{
	switch (state)
	{
	case RouterState::Undefined:
		PinConnectionStatus->StartFlash(20, 50, 2, 2000);
		break;
		
	case RouterState::Managed:
		PinConnectionStatus->StartFlash(1000, 1000, 0, 0); 
		break;
		
	case RouterState::ManagedConnected:
		PinConnectionStatus->Switch(true); 
		break;
		
	case RouterState::Master:
		PinConnectionStatus->StartFlash(1000, 1000, 0, 0); 
		break;
		
	case RouterState::MasterClientConnected:
		PinConnectionStatus->Switch(true); 
		break;
	}
}
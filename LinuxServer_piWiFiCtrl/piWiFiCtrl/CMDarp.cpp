#include <algorithm>


#include "CMDarp.h"
#include "Parser.h"
#include "UtilityFn.h"

using namespace std;


/////////////////////////////////////////////////////////////////////////////
//  CMDarpWiFiDevices
//  Command "arp"
//  parses out devices connected on wlan0, creates map of hardware address to device object
//

//  Constructor
//
CMDarpWiFiDevices::CMDarpWiFiDevices()
{
	Command = "arp";
	

}

//  Copy Constructor
//
CMDarpWiFiDevices::CMDarpWiFiDevices(CMDarpWiFiDevices& rhs) : CMD(rhs)
{
	*this = rhs;
}


//  Destructor
//
CMDarpWiFiDevices::~CMDarpWiFiDevices()
{
	DeleteMap(_ConnectedDevices);
}


//  Assignment Operator
//
CMDarpWiFiDevices& CMDarpWiFiDevices::operator=(CMDarpWiFiDevices& rhs)
{
	DeleteMap(_ConnectedDevices);

	CMD::operator=(rhs);

	map<string, ArpDevice*>::iterator nextItem;
	for (nextItem = rhs._ConnectedDevices.begin(); nextItem != rhs._ConnectedDevices.end(); ++nextItem)
	{
		ArpDevice* copyItem = new ArpDevice(*(nextItem->second));
		AddObjectToMap(nextItem->first, copyItem, _ConnectedDevices);
	}

	return *this;
}


//  ConnectedDevices()
//  returns a list of strings of hw address of connected devices on wlan0
//
vector<string> CMDarpWiFiDevices::ConnectedDevices()
{
	vector<string> devices;
	//  clean up our map
	map<string, ArpDevice*>::iterator nextItem;
	for (nextItem = _ConnectedDevices.begin(); nextItem != _ConnectedDevices.end(); ++nextItem)
	{
		devices.push_back(nextItem->second->HwAddress);
	}

	return devices;
}


//  ConnectedDevice
//  returns the ArpDevice object for the given hardware address
//
ArpDevice* CMDarpWiFiDevices::ConnectedDevice(string name)
{
	//  see if this client exists already
	map<string, ArpDevice*>::iterator it = _ConnectedDevices.find(name);
	if (it == _ConnectedDevices.end())
	{
		//  this key does not exist, not expected here
		return NULL;
	}

	return it->second;
}


//  Parse
//  override of base class CMD::Parse 
//  parse this command to create a map of hw address to an object for the device
//
bool CMDarpWiFiDevices::Parse()
{
	DeleteMap(_ConnectedDevices);

	if (CommandResponse.size() == 0)
		return false;

	ArpDevice* nextDevice = NULL;

	for (int i = 0; i < CommandResponse.size(); i++)
	{
		string nextString = CommandResponse[i];

		if (nextString.find("wlan0") != string::npos && nextString.find("(incomplete)") == string::npos)
		{
			AddObjectToMap(nextDevice != NULL ? nextDevice->HwAddress : "", nextDevice, _ConnectedDevices);

			nextDevice = new ArpDevice();
			//  this is a device connected to the wlan router
			Parser parseLine(nextString, " ");
			string addressOfDevice = parseLine.GetNextString();
			//  get rid of "
			addressOfDevice.erase(std::remove(addressOfDevice.begin(), addressOfDevice.end(), '"'), addressOfDevice.end());
			nextDevice->IpAddress = addressOfDevice;

			parseLine.GetNextString();
			nextDevice->HwAddress = parseLine.GetNextString();
			
		}
	}
	
	//  add last object to the map
	AddObjectToMap(nextDevice != NULL ? nextDevice->HwAddress : "", nextDevice, _ConnectedDevices);
	
	return true;
}







////////////////////////////////////////////////////////////////////////////////
//  CMDipNeighShow
//  Command "ip neigh show"
//  parses out devices connected on wlan0, creates map of hardware addresses to device object
//

//  Constructor
//
CMDipNeighShow::CMDipNeighShow()
{
	Command = "ip neigh show";


}

//  Copy Constructor
//
CMDipNeighShow::CMDipNeighShow(CMDipNeighShow& rhs) : CMD(rhs)
{
	*this = rhs;
}


//  Destructor
//
CMDipNeighShow::~CMDipNeighShow()
{
	DeleteMap(_ConnectedDevices);
}


//  Assignment Operator
//
CMDipNeighShow& CMDipNeighShow::operator=(CMDipNeighShow& rhs)
{
	DeleteMap(_ConnectedDevices);

	CMD::operator=(rhs);

	map<string, ArpDevice*>::iterator nextItem;
	for (nextItem = rhs._ConnectedDevices.begin(); nextItem != rhs._ConnectedDevices.end(); ++nextItem)
	{
		ArpDevice* copyItem = new ArpDevice(*(nextItem->second));
		AddObjectToMap(nextItem->first, copyItem, _ConnectedDevices);
	}

	return *this;
}


//  ConnecatedDevices()
//  returns a list of connected device hw addresses
//
vector<string> CMDipNeighShow::ConnectedDevices()
{
	vector<string> devices;
	//  clean up our map
	map<string, ArpDevice*>::iterator nextItem;
	for (nextItem = _ConnectedDevices.begin(); nextItem != _ConnectedDevices.end(); ++nextItem)
	{
		devices.push_back(nextItem->second->HwAddress);
	}

	return devices;
}



//  ConnectedDevice
//  returns the ArpDevice for the given hardware address
//
ArpDevice* CMDipNeighShow::ConnectedDevice(string name)
{
	//  see if this client exists already
	map<string, ArpDevice*>::iterator it = _ConnectedDevices.find(name);
	if (it == _ConnectedDevices.end())
	{
		//  this key does not exist, not expected here
		return NULL;
	}

	return it->second;
}


//  Parse
//  override of base class CMD::Parse 
//  parse this command to create a map of hw address to an object for the device
// 
//
bool CMDipNeighShow::Parse()
{
	DeleteMap(_ConnectedDevices);

	if (CommandResponse.size() == 0)
		return false;

	ArpDevice* nextDevice = NULL;

	for (int i = 0; i < CommandResponse.size(); i++)
	{
		string nextString = CommandResponse[i];

		if (nextString.find("lladdr") != string::npos && nextString.find("wlan0") != string::npos)
		{
			AddObjectToMap(nextDevice != NULL ? nextDevice->HwAddress : "", nextDevice, _ConnectedDevices);

			nextDevice = new ArpDevice();
			//  this is a device connected to the wlan router
			Parser parseLine(nextString, " ");
			string getNextString = parseLine.GetNextString();

			//  get rid of "
			getNextString.erase(std::remove(getNextString.begin(), getNextString.end(), '"'), getNextString.end());
			nextDevice->IpAddress = getNextString;
			parseLine.GetNextString();
			parseLine.GetNextString();
			parseLine.GetNextString();

			nextDevice->HwAddress = parseLine.GetNextString();
		}
	}

	//  add last object to the map
	AddObjectToMap(nextDevice != NULL ? nextDevice->HwAddress : "", nextDevice, _ConnectedDevices);

	return true;
}


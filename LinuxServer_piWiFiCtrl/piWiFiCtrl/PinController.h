#pragma once
#include <chrono>

typedef enum PinControlMode
{
	pcmUndefined,
	pcmBinary,
	pcmBlink,
} PinControlMode;
	
	
	
class PinController
{
public:
	PinController(int pinNumber);
	
	void StartFlash(int onMs, int offMs, int reps, int delayMs);
	void Switch(bool on);
	
	void Update();
	
	
protected:
	
	PinControlMode Mode;
	int PinNumber;
	int BlinkTimeOnMs;
	int BlinkTimeOffMs;
	int BlinkReps;
	int BlinkTimeDelayMs;
	int BlinkTimeWaiting;
	
	std::chrono::steady_clock::time_point SetTime;
};
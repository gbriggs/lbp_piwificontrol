#include <algorithm>

#include "CMDiwlistScan.h"
#include "Parser.h"
#include "UtilityFn.h"

using namespace std;





/////////////////////////////////////////////////////////////////////////////
//  CMDiwlistScan
//  Command "ifconfig"
//

//  Constructor
//
CMDiwlistScan::CMDiwlistScan()
{
	Command = "iwlist wlan0 scanning";

}


CMDiwlistScan::CMDiwlistScan(CMDiwlistScan& rhs) : CMD(rhs)
{
	*this = rhs;
}


CMDiwlistScan::~CMDiwlistScan()
{
	DeleteMap(_AccessPoints);
}


CMDiwlistScan& CMDiwlistScan::operator=( CMDiwlistScan& rhs)
{
	DeleteMap(_AccessPoints);

	CMD::operator=(rhs);

	map<string, WiFiAccessPoint*>::iterator nextItem;
	for (nextItem = rhs._AccessPoints.begin(); nextItem != rhs._AccessPoints.end(); ++nextItem)
	{
		WiFiAccessPoint* copyItem = new WiFiAccessPoint(*(nextItem->second));
		AddObjectToMap(nextItem->first, copyItem, _AccessPoints);
	}

	return *this;
}



vector<string> CMDiwlistScan::AccessPoints()
{
	vector<string> aps;
	//  build a vector of strings from essid name of all access points
	map<string, WiFiAccessPoint*>::iterator nextItem;
	for (nextItem = _AccessPoints.begin(); nextItem != _AccessPoints.end(); ++nextItem)
	{
		aps.push_back(nextItem->second->Essid);
	}

	return aps;
}


vector<string> CMDiwlistScan::VisibleAccessPoints()
{
	vector<string> aps;
	//  build a vector of strings from essid name of all access points
	map<string, WiFiAccessPoint*>::iterator nextItem;
	for (nextItem = _AccessPoints.begin(); nextItem != _AccessPoints.end(); ++nextItem)
	{
		if (nextItem->second->Quality < 10 || abs(nextItem->second->SignalLevel) < 10)
			continue;

		aps.push_back(nextItem->second->Essid);
	}

	return aps;
}


WiFiAccessPoint* CMDiwlistScan::AccessPoint(string name)
{
	//  see if access point exists
	map<string, WiFiAccessPoint*>::iterator it = _AccessPoints.find(name);
	if (it == _AccessPoints.end())
	{
		//  this key does not exist, not expected here
		return NULL;
	}

	return it->second;
}



//  Parse
//  override of base class CMD::Parse to extract specific bits of the ifconfig response
//
bool CMDiwlistScan::Parse()
{
	//  clean up old list
	DeleteMap(_AccessPoints);

	Parser parser("", "");

	WiFiAccessPoint* nextAccessPoint = NULL;

	for ( int i = 0; i < CommandResponse.size(); i++ )
	{
		string nextString = CommandResponse[i];

		if (nextString.find("Cell ") != string::npos && nextString.find(" - Address") != string::npos)
		{
			//  add our previous access point to the list
			AddObjectToMap( nextAccessPoint != NULL ? nextAccessPoint->Essid : "",  nextAccessPoint, _AccessPoints);

			nextAccessPoint =  new WiFiAccessPoint();
			nextAccessPoint->Address = nextString;
		}
		else if (nextAccessPoint != NULL && nextString.find("ESSID:") != string::npos)
		{
			Parser parseLine = Parser(nextString, ":");
			parseLine.GetNextString();
			nextAccessPoint->Essid = parseLine.GetNextString();
			//  get rid of "
			nextAccessPoint->Essid.erase(std::remove(nextAccessPoint->Essid.begin(), nextAccessPoint->Essid.end(), '"'), nextAccessPoint->Essid.end());
		}
		else if (nextAccessPoint != NULL && nextString.find("Protocol:") != string::npos)
		{
			Parser parseLine = Parser(nextString, ":");
			parseLine.GetNextString();
			nextAccessPoint->Protocol = parseLine.GetNextString();
		}
		else if (nextAccessPoint != NULL && nextString.find("Frequency:") != string::npos)
		{
			Parser parseLine = Parser(nextString, ":");
			parseLine.GetNextString();
			nextAccessPoint->Frequency = parseLine.GetNextString();
		}
		else if (nextAccessPoint != NULL && nextString.find("Encryption key:") != string::npos)
		{
			Parser parseLine = Parser(nextString, ":");
			parseLine.GetNextString();
			nextAccessPoint->Encrypted = parseLine.GetNextString().compare("on") == 0;
		}
		else if (nextAccessPoint != NULL && nextString.find("Bit Rates:") != string::npos)
		{
			Parser parseLine = Parser(nextString, ":");
			parseLine.GetNextString();
			nextAccessPoint->BitRate = parseLine.GetNextString();
		}
		else if (nextAccessPoint != NULL && nextString.find("Quality=") != string::npos)
		{
			Parser parseLine = Parser(nextString, "=/");
			parseLine.GetNextString();
			nextAccessPoint->Quality = parseLine.GetNextInt();
			parseLine.GetNextString();
			nextAccessPoint->SignalLevel = parseLine.GetNextInt();
		}
	}

	//  add the pending access point we are creating
	AddObjectToMap(nextAccessPoint != NULL ? nextAccessPoint->Essid : "", nextAccessPoint, _AccessPoints);

	return true;
}


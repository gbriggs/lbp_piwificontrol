#include <iostream>

#include "WiFiControl.h"
#include "GpioControl.h"


using namespace std;

WiFiControl WiFiControl;

int PinNumberConnectionStatus = 0;
bool parse_args(int argc, char *argv[]);


//  Main function
//
int main(int argc, char *argv[])
{
	if (!parse_args(argc, argv))
		return -1;
	
	StartGpioController(PinNumberConnectionStatus);
	SetConnectionLed(RouterState::Undefined);
	
	//  startup the app
	if (!WiFiControl.Start())
	{
		printf("! Failed to start application.");
		return 1;
	}

	while ( WiFiControl.IsRunning())
	{
		Sleep(1000);
	}
	
	StopGpioController();
}



//  Parse the command line args from the brainflow sample
//
bool parse_args(int argc, char *argv[])
{
	for (int i = 1; i < argc; i++)
	{
		
		if (std::string(argv[i]) == std::string("--pin-conn"))
		{
			if (i + 1 < argc)
			{
				i++;
				PinNumberConnectionStatus = std::stoi(std::string(argv[i]));
			}
			else
			{
				std::cerr << "missed argument" << std::endl;
				return false;
			}
		}
		
	}
	
	return true;
}
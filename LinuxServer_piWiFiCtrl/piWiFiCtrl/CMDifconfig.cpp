#include "CMDifconfig.h"
#include "Parser.h"

using namespace std;


/////////////////////////////////////////////////////////////////////////////
//  CMDifconfig
//  Command "ifconfig"
//

//  Constructor
//
CMDifconfig::CMDifconfig()
{
	Command = "ifconfig";

}

CMDifconfig::~CMDifconfig()
{
}


//  Parse
//  override of base class CMD::Parse to extract specific bits of the ifconfig response
//
bool CMDifconfig::Parse()
{
	Parser parser("", "");

	for ( int i = 0; i < CommandResponse.size(); i++ )
	{

		string nextString = CommandResponse[i];

		parser.SetBuffer(nextString, " ");

		string getString = parser.GetNextString();
		while ( getString.size() != 0 )
		{
			if ( getString == "eth0:" )
			{
				_Eth0Info.Type = getString;
				
				nextString = CommandResponse[++i];
				parser.SetBuffer(nextString, " :");

				//  discard inet 
				getString = parser.GetNextString();

				//  parse the IP address
				_Eth0Info.Inet4Address = parser.GetNextString();

				//  done with this connection
				break;
			}
			else if ( getString == "lo:" )
			{
				_LoopbackInfo.Type = getString;

				nextString = CommandResponse[++i];
				parser.SetBuffer(nextString, " :");

				//  discard inet
				getString = parser.GetNextString();

				//  parse the IP address
				_LoopbackInfo.Inet4Address = parser.GetNextString();

				//  done with this connection
				break;
			}
			else if ( getString == "wlan0:" )
			{
				_WlanInfo.Type = getString;
				
				nextString = CommandResponse[++i];
				parser.SetBuffer(nextString, " :");

				//  discard inet
				getString = parser.GetNextString();

				//  parse the IP address
				_WlanInfo.Inet4Address = parser.GetNextString();

				//  done with this connection
				break;
			}
			else
			{
				break;
			}
		}
	}


	return true;
}


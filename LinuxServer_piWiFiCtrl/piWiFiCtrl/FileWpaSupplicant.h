#ifndef _FILE_WPASUPPLICANT
#define _FILE_WPASUPPLICANT

#include <string>
#include <map>
#include <vector>

using namespace std;

class WpaNetwork
{
public:
	WpaNetwork();

	string SsidKey();
	string SsidValue();
	void SsidKeyValuePair(string kvPair) { _SsidKeyValuePair = kvPair; }

	void PasswordKeyValuePair(string kvPair) { _PasswordKeyValuePair = kvPair; }

	string PasswordKey();
	string PasswordValue();
	void SetPassword(string password);
	

	vector<string> UnparsedKeyValuePairs;

protected:

	string _SsidKeyValuePair;
	string _PasswordKeyValuePair;

	
};


// FileWpaSupplicant
// parses the file /etc/wpa_supplicant.conf
//

class FileWpaSupplicant 
{
public:
	FileWpaSupplicant();

	virtual ~FileWpaSupplicant();

	//  parse the file
	bool ParseFile();
	bool UpdateFile(string accessPoint, string password);

	vector<string> WpaNetworks();
	WpaNetwork* Network(string name);
	
protected:

	vector<string> _Comments;

	std::map<std::string, WpaNetwork*> _WpaNetworks;

};



#endif
#ifndef _WiFiControl_H
#define _WiFiControl_H

#include <string>
#include <list>
#include <mutex>
#include <map>

#include "Thread.h"
#include "ConnectionThread.h"
#include "ControlThread.h"
#include "NetworkMonitorThread.h"

#include "CMDifconfig.h"
#include "UtilityFn.h"
#include "CMDiwconfig.h"
#include "CMDiwlistScan.h"
#include "CMDhostapd.h"
#include "CMDarp.h"

using namespace std;

typedef enum RouterState
{
	Undefined,
	Master,
	MasterClientConnected,
	Managed,
	ManagedConnected,
} RouterState;




// WiFiControl
// This class is the main application behavior
// This class has two threads:
//    -  Main thread:  monitors the state of the wifi and automatically switches between router / wifi client mode
//    -  Socket server thread:  listens on server port and responds to GET / PUT / DELETE requests
//
class WiFiControl
{
public:

	WiFiControl();
	virtual ~WiFiControl();

	//  initialize app components for startup
	bool Start();

	//  shut down application components for exit
	void ShutDown();

	//  Main thread run state
	bool IsRunning() { return Running; }

	//  Properties
	CMDiwconfig WiFiState();
	string RouterMode();
	CMDhostapdAllsta WiFiConnections();
	CMDipNeighShow WiFiDevices();
	vector<string> KnownNetworks();
	CMDiwlistScan AvailableNetworks();

	string RouterStatus();

	bool ControlBusy() { return _ControlThread.IsBusy(); }

	bool RequestCommand(string command);

	//  Control functions
	bool SetMode(iwcMode mode);
	bool SetAccessPoint(string accessPoint, string wpaFile);
	
	

protected:
	
	//  Main run loop
	//  monitors and manages state of wifi connection
	bool Running;

	NetworkMonitorThread _NetworkMonitorThread;
	friend class NetworkMonitorThread;

	//  remember time of last connection
	timeval TimeOfLastConnection;

	//  properties of 
	CMDiwconfig _WiFiState;
	CMDhostapdAllsta _WiFiConnections;
	//CMDarpWiFiDevices _WiFiDevices;
	CMDipNeighShow _WiFiDevices;
	vector<string> _KnownNetworks;
	CMDiwlistScan _AvailableNetworks;

	//  Properties access
	timed_mutex LockData;

	//  Server Connection Thread
	//
	ConnectionThread _ConnectionThread;
	//  connection server port
	int ServerPort;

	//  Control thread
	ControlThread _ControlThread;

	//  Control access
	timed_mutex LockControl;

	//  Control functions
	void SwitchToRouterMode();
	void SwitchToClientMode();
	RouterState CurrentRouterState;
	
	void CheckRouterState(RouterState state);

	//  versioning
	string VersionString;
};


#define SYSEVENT "systemEvent"


#endif // _WiFiControl_H
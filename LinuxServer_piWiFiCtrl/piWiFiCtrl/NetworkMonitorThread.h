#pragma once
#include "Thread.h"

//  Helper classe for the main app
//

class WiFiControl;


//  NetworkMonitorThread
//  a thread running to monitor the state of the network connection
//
class NetworkMonitorThread : public Thread
{
public:
	// this class takes a reference to the WiFiControl class
	// this is a quick and dirty (and easy) way for classes to communicate without using singletons or global variables
	NetworkMonitorThread(WiFiControl& wiFiControl);

	virtual void RunFunction();

	timeval TimeOfLastConnection;
	timeval TimeOfLastSwitchToRouterMode;

protected:
	//  reference to WiFiControl object so we can call its functions
	WiFiControl& _WiFiControl;
}
;


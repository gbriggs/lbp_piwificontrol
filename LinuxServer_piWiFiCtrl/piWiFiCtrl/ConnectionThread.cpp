#include <unistd.h>
#include <string>
#include <sys/time.h>
#include <netinet/in.h>
#include "string"
#include <iostream>

#include "ConnectionThread.h"
#include "WiFiControl.h"
#include "Parser.h"
#include "UtilityFn.h"
#include "CMDiwconfig.h"
#include "CMDhostapd.h"
#include "CMDarp.h"
#include "FileWpaSupplicant.h"
#include "CMDiwconfig.h"
#include "CMDiwlistScan.h"

using namespace std;




/////////////////////////////////////////////////////////////////////////////
//  ConnectionThread
//  manages the main server connection thread, listens for connection requests from clients
//


ConnectionThread::ConnectionThread(WiFiControl& WiFiControl) :
_WiFiControl(WiFiControl)
{
}


ConnectionThread::~ConnectionThread()
{
	if (ThreadRunning)
	{
		Cancel();
	}
}




string FormatList(vector<string> listToFormat)
{
	string listFormatted = "";

	//  get available networks
	for (int i = 0; i < listToFormat.size(); i++)
		listFormatted += format("%s\n", listToFormat[i].c_str());
	
	listFormatted += "\n";
	
	return listFormatted;
}


//--------------------------------
//  RunFunction
//  this is the Thread base class override
//  this function gets called each cycle through the thread
void ConnectionThread::RunFunction()
{
	while (IsRunning())
	{
		
		struct sockaddr_in clientAddress;
		string readFromSocket;

		cout << ">>]  Connection Thread socket waiting: " << readFromSocket << endl;

		int acceptFileDescriptor = ReadStringFromSocket(&clientAddress, readFromSocket);

		if (acceptFileDescriptor < 0)
			continue;

		cout << ">>  Connection Thread socket accept: " << readFromSocket << endl;

		//  log event time
		timeval eventTime;
		gettimeofday(&eventTime, 0);

		//  log event sender
		char* addressOfSender = inet_ntoa(clientAddress.sin_addr);
		string eventSender = string(addressOfSender);

		//  parse the read string for known commands
		//  command syntax is "$TCP_SOMECOMMAND,argument1,argument2,...
		//
		Parser readParser(readFromSocket, ",");
		string command = readParser.GetNextString();

		//  Look for recognized commands
		//
		if(command.compare("$LBP_GET_STATUS") == 0)
		{
			string status = "";
			if (_WiFiControl.ControlBusy())
			{
				status = "$LBP_GET_STATUS,NAK,busy";
				usleep(1000000);
			}
			else
				status = format("$LBP_GET_STATUS,ACK,%s\n", _WiFiControl.RouterStatus().c_str());
		
			//  write response
			WriteStringToSocket(acceptFileDescriptor, status);
			continue;
		}
		else if(command.compare("$LBP_GET_MODE") == 0)
		{
			string status = "";
			if (_WiFiControl.ControlBusy())
			{
				status = "$LBP_GET_MODE,NAK,busy\n";
				usleep(1000000);
			}
			else
				status = format("$LBP_GET_MODE,ACK,%s\n", _WiFiControl.RouterMode().c_str());
		
			//  write response
			WriteStringToSocket(acceptFileDescriptor, status);

		}
		else if(command.compare("$LBP_GET_APS") == 0)
		{
			//  format state string
			string aps = "";
			if (_WiFiControl.ControlBusy())
			{			
				aps = "$LBP_GET_APS,NAK,busy\n";
				usleep(1000000);
			}
			else
				aps = format("$LBP_GET_APS,ACK,%s\n", FormatList(_WiFiControl.AvailableNetworks().AccessPoints()).c_str());

			//  write response
			WriteStringToSocket(acceptFileDescriptor, aps);
		}
		else if(command.compare("$LBP_SET_AP") == 0)
		{
			string response = "";
		
			if (_WiFiControl.RequestCommand(readFromSocket))
				response = "$LBP_SET_AP,ACK,Setting access point ...\n";
			else
			{
				response = "$LBP_SET_AP,NAK,The Pi is already busy setting the mode, please try again later\n";
			}

			//  write response
			WriteStringToSocket(acceptFileDescriptor, response);
		
		}
		else if(command.compare("$LBP_SET_MODE") == 0)
		{
			string response = "";
			if (_WiFiControl.RequestCommand(readFromSocket))
				response = "$LBP_SET_MODE,ACK,Setting WiFi mode ...\n";
			else
			{
				response = "$LBP_SET_MODE_NAK,The Pi is already busy setting the mode, please try again later\n";
			}

			WriteStringToSocket(acceptFileDescriptor, response);
		}
		else
		{
			//  unknown command
			string returnMessage = format("LBP_NAK,unknown command: %s\n", readFromSocket.c_str());
			write(acceptFileDescriptor, returnMessage.c_str(), returnMessage.size());
		}
	}
}
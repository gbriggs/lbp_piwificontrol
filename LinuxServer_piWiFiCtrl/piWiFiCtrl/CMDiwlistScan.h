#ifndef _CMDIWLISTSCAN_H
#define _CMDIWLISTSCAN_H

#include <string>
#include <map>
#include <vector>

#include "CMD.h"


using namespace std;


//  ConnectionInfo
//  container class for internet information
//
class WiFiAccessPoint
{
public:

	WiFiAccessPoint()
	{
		Address = "";
		Essid = "";
		Protocol = "";
		Frequency = "";
		Mode = "";
		Encrypted = false;
		BitRate = "";
		Quality = 0;
		SignalLevel = 0;
	}

	bool IsVisible() { return Quality > 10 && abs(SignalLevel) > 10; }

	string Address;
	string Essid;
	string Protocol;
	string Frequency;
	string Mode;
	bool   Encrypted;
	string BitRate;
	int Quality;
	int SignalLevel;
};



//  deletion helper function
//inline static bool deleteWiFiAccessPoint(WiFiAccessPoint* objectToDelete) { delete objectToDelete; return true; }


// CMDifconfig
// Command "ifconfig"
//
class CMDiwlistScan : public CMD
{
public:
	CMDiwlistScan();
	CMDiwlistScan(CMDiwlistScan& rhs);

	virtual ~CMDiwlistScan();

	CMDiwlistScan& operator=(CMDiwlistScan& rhs);

	//  override base class CMD::Parse so we can parse internet address out of system response
	virtual bool Parse();

	vector<string> AccessPoints();
	vector<string> VisibleAccessPoints();
	WiFiAccessPoint* AccessPoint(string name);

protected:
	map<string,WiFiAccessPoint*> _AccessPoints;
};


#endif
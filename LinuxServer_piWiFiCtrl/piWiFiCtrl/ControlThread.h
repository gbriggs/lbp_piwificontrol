#ifndef _CONTROLTHREAD_H
#define _CONTROLTHREAD_H

#include <condition_variable>
#include <queue>

#include "Thread.h"
#include "UtilityFn.h"

class WiFiControl;

using namespace std;

//
//  ControlThread
//  a thread that waits for messages to be added to the queue,
//  then broadcasts messages to all clients
//

class ControlThread : public Thread
{
public:

	ControlThread(WiFiControl& WiFiControl);
	
	virtual ~ControlThread();

	//  IsBusy, true when control thread is busy processing a command
	bool IsBusy() { return _IsBusy; }
	//  signal the control thread to do something
	//  will return false if it is busy doing a command already
	bool ControlCommand(string command);

	//  override of Thread::Cancel so we can wake thread up before stopping run function
	virtual void Cancel();

	//  RunFunction
	virtual void RunFunction();

protected:

	// the command to execute
	string Command;

	//  simple mechanism to enforce only one command at a time
	bool _IsBusy;

	//  condition variable is used for notification of thread to wake up
	bool Notified;
	std::condition_variable NotifyMessagesCondition;
	std::mutex NotifyMutex;

	void Notify();

	//  reference to WiFiControl
	WiFiControl& _WiFiControl;
};





#endif //_CONTROLTHREAD_H
#include <algorithm>
#include <fstream>


#include "FileWpaSupplicant.h"
#include "Parser.h"
#include "UtilityFn.h"

using namespace std;

string ParseValue(string keyValuePair)
{
	//  this is a device connected to the wlan router
	Parser parseLine(keyValuePair, "=");
	parseLine.GetNextString();
	string value = parseLine.GetNextString();
	//  get rid of "
	value.erase(std::remove(value.begin(), value.end(), '"'), value.end());
	value.erase(std::remove(value.begin(), value.end(), '\t'), value.end());
	return value;
}


string ParseKey(string keyValuePair)
{
	//  this is a device connected to the wlan router
	Parser parseLine(keyValuePair, "=");
	return parseLine.GetNextString();
}


WpaNetwork::WpaNetwork()
{
	_SsidKeyValuePair = "ssid=\"\"";
	_PasswordKeyValuePair = "psk=\"\"";
}


string WpaNetwork::SsidKey()
{
	return ParseKey(_SsidKeyValuePair);
}


string WpaNetwork::SsidValue()
{
	return ParseValue(_SsidKeyValuePair);
}


void WpaNetwork::SetPassword(string password)
{
	_PasswordKeyValuePair = format("%s=\"%s\"", PasswordKey().c_str(), password.c_str());
}
string WpaNetwork::PasswordKey()
{
	return ParseKey(_PasswordKeyValuePair);
}


string WpaNetwork::PasswordValue()
{
	string pwKey = ParseKey(_PasswordKeyValuePair);
	string password = ParseValue(_PasswordKeyValuePair);

	return format("\"%s\"", password.c_str());
}





/////////////////////////////////////////////////////////////////////////////
//  FileWpaSupplicant
//  
//

//  Constructor
//
FileWpaSupplicant::FileWpaSupplicant()
{

}


//void DeleteMap(map<string, WpaNetwork*> &deleteMap)
//{
//	//  clean up our map
//	map<string, WpaNetwork*>::iterator nextItem;
//	for (nextItem = deleteMap.begin(); nextItem != deleteMap.end(); ++nextItem)
//	{
//		delete nextItem->second;
//	}
//}

FileWpaSupplicant::~FileWpaSupplicant()
{
	DeleteMap(_WpaNetworks);
}





vector<string> FileWpaSupplicant::WpaNetworks()
{
	vector<string> networks;
	//  clean up our map
	map<string, WpaNetwork*>::iterator nextItem;
	for (nextItem = _WpaNetworks.begin(); nextItem != _WpaNetworks.end(); ++nextItem)
	{
		networks.push_back(nextItem->second->SsidValue());
	}

	return networks;
}


WpaNetwork* FileWpaSupplicant::Network(string name)
{
	//  see if this client exists already
	map<string, WpaNetwork*>::iterator it = _WpaNetworks.find(name);
	if (it == _WpaNetworks.end())
	{
		//  this key does not exist, not expected here
		return NULL;
	}

	return it->second;
}




//  Parse
//  override of base class CMD::Parse to extract specific bits of the ifconfig response
//
bool FileWpaSupplicant::ParseFile()
{
	//  clean up our old collection
	DeleteMap(_WpaNetworks);
	_Comments.clear();
	
	//  open the header file
	ifstream wpaFile;
	wpaFile.open("/etc/wpa_supplicant/wpa_supplicant.conf");
	if (!wpaFile.is_open())
	{
		return false;
	}

	//  read the header file, create a WpaNetwork for each network
	WpaNetwork* nextNetwork = NULL;

	//  read the contents of the header file
	bool endHeader;
	string readLine;
	while (!wpaFile.eof())
	{
		getline(wpaFile, readLine);
		
		if (readLine.find("network={") != string::npos)
		{
			endHeader = true;
			if (nextNetwork != NULL)
				delete nextNetwork;

			nextNetwork = new WpaNetwork();
		}
		else if (nextNetwork != NULL && readLine.find("ssid=") != string::npos)
		{
			nextNetwork->SsidKeyValuePair(readLine);
		}
		else if (nextNetwork != NULL &&  readLine.find("psk=") != string::npos)
		{
			nextNetwork->PasswordKeyValuePair(readLine);
		}
		else if (nextNetwork != NULL &&  readLine.find("}") != string::npos)
		{
			//  add any pending wpa network to the map
			AddObjectToMap(nextNetwork != NULL ? nextNetwork->SsidValue() : "", nextNetwork, _WpaNetworks);
			nextNetwork = NULL;
		}
		else if (nextNetwork != NULL)
		{
			//  add this to the list of things we don't parse
			nextNetwork->UnparsedKeyValuePairs.push_back(readLine);
		}
		else if (!endHeader && readLine.length() > 0)
		{
			_Comments.push_back(readLine);
		}
	}

	if (nextNetwork != NULL)
		delete nextNetwork;

	
	return true;
}




void WriteNetwork(ofstream& file, WpaNetwork* network)
{
	//  open the network
	file << "network={\n";
	//  write ssid
	file << "ssid=\"" << network->SsidValue() << "\"\n";
	//  write password
	file << network->PasswordKey() << "=" << network->PasswordValue() << "\n";

	//  write out all the rest of the stuff
	vector<string>::iterator nextString;
	for (nextString = network->UnparsedKeyValuePairs.begin(); nextString != network->UnparsedKeyValuePairs.end(); ++nextString)
		file << *nextString << "\n";

	//  close the network
	file << "}\n";
	file << "\n\n";
}




bool FileWpaSupplicant::UpdateFile(string accessPoint, string password)
{
	//  open the header file
	ofstream wpaFile;
	wpaFile.open("/etc/wpa_supplicant/wpa_supplicant.conf");
	if (!wpaFile.is_open())
	{
		return false;
	}
	
	
	wpaFile << "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\n";
	wpaFile << "update_config=1\n";
	wpaFile << "country=US\n";
	
	//  write the header from the original parsed file, should be like format above	
	//  this is not working, use hard coded header above
//	vector<string>::iterator nextComment;
//	for (nextComment = _Comments.begin(); nextComment != _Comments.end(); ++nextComment)
//		wpaFile << *nextComment << "\n";
//	
	//  cycle through all the known networks, see if we are making a new one or not
	WpaNetwork* existingNetwork = NULL;

	map<string, WpaNetwork*>::iterator nextItem;
	for (nextItem = _WpaNetworks.begin(); nextItem != _WpaNetworks.end(); ++nextItem)
	{
		if (nextItem->second->SsidValue().compare(accessPoint) == 0)
		{
			//  found our network
			existingNetwork = nextItem->second;
			break;
		}
	}

	
	//  write out all the networks, write our newly selected one first
	if (existingNetwork != NULL)
	{
		//  reset the password if new password is not empty
		if ( password.length() > 0 )
			existingNetwork->SetPassword(password);

		WriteNetwork(wpaFile, existingNetwork);
	}
	else
	{
		//  We a new network
		wpaFile << "network={\n";
		wpaFile << "ssid=\"" << accessPoint << "\"\n";
		wpaFile << "psk=\"" << password << "\"\n";
		wpaFile << "}\n";
	}

	//  now write the rest of the networks
	for (nextItem = _WpaNetworks.begin(); nextItem != _WpaNetworks.end(); ++nextItem)
	{
		if (nextItem->second == existingNetwork)
		{
			//  found our network
			existingNetwork = nextItem->second;
			continue;
		}

		WriteNetwork(wpaFile, nextItem->second);
	}
	
	

	wpaFile.close();

	
	return true;
}

#include "CMDhostapd.h"
#include "Parser.h"
#include "UtilityFn.h"

using namespace std;


/////////////////////////////////////////////////////////////////////////////
//  CMDhostapdCtrl
//  Command "ifconfig"
//

//  Constructor
//
CMDhostapdCtrl::CMDhostapdCtrl()
{
	Command = "service hostapd status";
	_HostApdRunning = false;

}

CMDhostapdCtrl::~CMDhostapdCtrl()
{
	
}


//  Parse
//  override of base class CMD
//  parse hostapd running status, on or off
//
bool CMDhostapdCtrl::Parse()
{

	_HostApdRunning = false;

	if (CommandResponse.size() == 0)
		return false;

	if (CommandResponse[0].find("hostapd is not running ... failed!") != string::npos)
		_HostApdRunning = false;
	else if (CommandResponse[0].find("hostapd is running") != string::npos)
		_HostApdRunning = true;
	else
		return false;
	
	return true;
}




HostapdStation::~HostapdStation()
{
	return;
}

/////////////////////////////////////////////////////////////////////////////
//  CMDhostapdAllsta
//  Command "hostapd_cli all_sta"
//

//  Constructor
//
CMDhostapdAllsta::CMDhostapdAllsta()
{
	Command = "hostapd_cli all_sta";


}

CMDhostapdAllsta::CMDhostapdAllsta(CMDhostapdAllsta& rhs) : CMD(rhs)
{
	*this = rhs;
}


CMDhostapdAllsta::~CMDhostapdAllsta()
{
	DeleteMap(_ConnectedStations);
}


CMDhostapdAllsta& CMDhostapdAllsta::operator=(CMDhostapdAllsta& rhs)
{
	DeleteMap(_ConnectedStations);

	CMD::operator=(rhs);

	map<string, HostapdStation*>::iterator nextItem;
	for (nextItem = rhs._ConnectedStations.begin(); nextItem != rhs._ConnectedStations.end(); ++nextItem)
	{
		HostapdStation* copyItem = new HostapdStation(*(nextItem->second));
		AddObjectToMap(nextItem->first, copyItem, _ConnectedStations);
	}
	
	return *this;
}


vector<string> CMDhostapdAllsta::ConnectedStations()
{
	vector<string> stations;
	//  clean up our map
	map<string, HostapdStation*>::iterator nextItem;
	for (nextItem = _ConnectedStations.begin(); nextItem != _ConnectedStations.end(); ++nextItem)
	{
		stations.push_back(nextItem->second->Address);
	}

	return stations;
}


HostapdStation* CMDhostapdAllsta::ConnectedStation(string name)
{
	//  see if this client exists already
	map<string, HostapdStation*>::iterator it = _ConnectedStations.find(name);
	if (it == _ConnectedStations.end())
	{
		//  this key does not exist, not expected here
		return NULL;
	}

	return it->second;
}


//  Parse
//  override of base class CMD::Parse to extract specific bits of the ifconfig response
//
bool CMDhostapdAllsta::Parse()
{
	DeleteMap(_ConnectedStations);

	if (CommandResponse.size() == 0)
		return false;

	HostapdStation* nextStation = NULL;

	for (int i = 0; i < CommandResponse.size(); i++)
	{
		string nextString = CommandResponse[i];

		if (nextString.find("dot11RSNAStatsSTAAddress") != string::npos)
		{
			//  add previous station to the map
			AddObjectToMap(nextStation != NULL ? nextStation->Address : "", nextStation, _ConnectedStations);

			//  create new station
			nextStation = new HostapdStation();

			//  parse the address out of this line
			Parser parseLine(nextString, "=");
			parseLine.GetNextString();
			nextStation->Address = parseLine.GetNextString();
		}
		else if (nextStation != NULL && nextString.find("dot11RSNAStatsVersion") != string::npos)
		{
			Parser parseLine(nextString, "=");
			parseLine.GetNextString();
			nextStation->Version = parseLine.GetNextString();
		}
		else if (nextStation != NULL && nextString.find("dot11RSNAStatsSelectedPairwiseCipher") != string::npos)
		{
			Parser parseLine(nextString, "=");
			parseLine.GetNextString();
			nextStation->SelectedPairwiseCipher = parseLine.GetNextString();
		}
		else if (nextStation != NULL && nextString.find("dot11RSNAStatsTKIPLocalMICFailures") != string::npos)
		{
			Parser parseLine(nextString, "=");
			parseLine.GetNextString();
			nextStation->TkipLocalMicFailures = parseLine.GetNextString();
		}
		else if (nextStation != NULL && nextString.find("dot11RSNAStatsTKIPRemoteMICFailures") != string::npos)
		{
			Parser parseLine(nextString, "=");
			parseLine.GetNextString();
			nextStation->TkopRemoteMicFailures = parseLine.GetNextString();
		}
		else if (nextStation != NULL && nextString.find("hostapdWPAPTKState") != string::npos)
		{
			Parser parseLine(nextString, "=");
			parseLine.GetNextString();
			nextStation->WpaptkState = parseLine.GetNextString();
		}
		else if (nextStation != NULL && nextString.find("hostapdWPAPTKGroupState") != string::npos)
		{
			Parser parseLine(nextString, "=");
			parseLine.GetNextString();
			nextStation->WpaptkGroupState = parseLine.GetNextString();
		}
	}

	//  add last object to the map
	AddObjectToMap(nextStation != NULL ? nextStation->Address : "", nextStation, _ConnectedStations);

	return true;
}

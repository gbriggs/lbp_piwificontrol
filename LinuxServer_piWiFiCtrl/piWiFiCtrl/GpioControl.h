#pragma once

#include "WiFiControl.h"
#include "PinController.h"


void StartGpioController(int pinConnection);
void StopGpioController();

void SetConnectionLed(RouterState state);
